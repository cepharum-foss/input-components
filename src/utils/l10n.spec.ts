import {Localization} from "./l10n";

describe("L10n support", () => {
	describe("accepts as locale identifier", () => {
		it("en", () => {
			expect( Localization.isLocale( "en" ) ).toBeTruthy();
		});

		it("EN", () => {
			expect( Localization.isLocale( "EN" ) ).toBeTruthy();
		});

		it("de", () => {
			expect( Localization.isLocale( "de" ) ).toBeTruthy();
		});

		it("en-us", () => {
			expect( Localization.isLocale( "en-us" ) ).toBeTruthy();
		});

		it("en-US", () => {
			expect( Localization.isLocale( "en-US" ) ).toBeTruthy();
		});

		it("en_gb", () => {
			expect( Localization.isLocale( "en_gb" ) ).toBeTruthy();
		});

		it("en_GB", () => {
			expect( Localization.isLocale( "en_GB" ) ).toBeTruthy();
		});

		it("en_GB@utf8", () => {
			expect( Localization.isLocale( "en_GB@utf8" ) ).toBeTruthy();
		});

		it("en-GB@UTF-8", () => {
			expect( Localization.isLocale( "en-GB@UTF-8" ) ).toBeTruthy();
		});

		it("en@utf8", () => {
			expect( Localization.isLocale( "en@utf8" ) ).toBeTruthy();
		});

		it("en@UTF-8", () => {
			expect( Localization.isLocale( "en@UTF-8" ) ).toBeTruthy();
		});
	});

	describe("rejects as locale identifier", () => {
		it("<empty string>", () => {
			expect( Localization.isLocale( "" ) ).toBeFalsy();
		});

		it("*", () => {
			expect( Localization.isLocale( "*" ) ).toBeFalsy();
		});

		it("english", () => {
			expect( Localization.isLocale( "english" ) ).toBeFalsy();
		});

		it("ENG", () => {
			expect( Localization.isLocale( "ENG" ) ).toBeFalsy();
		});

		it("1", () => {
			expect( Localization.isLocale( "1" ) ).toBeFalsy();
		});
	});

	describe("translates", () => {
		it("missing translation into undefined", () => {
			const key = "SOME_LOOKUP";
			const map = Localization.current();
			const result = map.translate( key, { warnIfMissing: false } )

			expect( result ).toBeUndefined();
		});

		it("previously registered translation", () => {
			const key = "SOME_LOOKUP";
			const map = Localization.current();
			map.addTranslation("en", "SOME_LOOKUP", "foo")
			const result = map.translate( key, { warnIfMissing: false } )

			expect( result ).toEqual("foo");
		});

		it("previously registered translation for more generic locale than current one", () => {
			const key = "SOME_LOOKUP";
			const map = Localization.current();
			map.addTranslation("en", "SOME_LOOKUP", "foo")
			map.locale = "en-gb";
			const result = map.translate( key, { warnIfMissing: false } )

			expect( result ).toEqual("foo");
		});

		it("previously registered translation for most generic locale than current one", () => {
			const key = "SOME_LOOKUP";
			const map = Localization.current();
			map.addTranslation("en", "SOME_LOOKUP", "foo")
			map.locale = "en-gb@utf8";
			const result = map.translate( key, { warnIfMissing: false } )

			expect( result ).toEqual("foo");
		});

		it("previously registered translation for less specific locale than current one", () => {
			const key = "SOME_LOOKUP";
			const map = Localization.current();
			map.addTranslation("en-gb", "SOME_LOOKUP", "foo")
			map.locale = "en-gb@utf8";
			const result = map.translate( key, { warnIfMissing: false } )

			expect( result ).toEqual("foo");
		});

		it("previously registered translation for less specific locale than current one preferring over the one for most generic fallback", () => {
			const key = "SOME_LOOKUP";
			const map = Localization.current();
			map.addTranslation("en-gb", "SOME_LOOKUP", "foo")
			map.addTranslation("en", "SOME_LOOKUP", "bar")
			map.locale = "en-gb@utf8";
			const result = map.translate( key, { warnIfMissing: false } )

			expect( result ).toEqual("foo");
		});

		it("previously registered translation for more specific locale than current one into undefined", () => {
			const key = "SOME_LOOKUP";
			const map = Localization.current();
			map.addTranslation("en-gb", "SOME_LOOKUP", "foo")
			map.locale = "en";
			const result = map.translate( key, { warnIfMissing: false } )

			expect( result ).toBeUndefined();
		});

		it("previously registered translation for different locale than current one into undefined", () => {
			const key = "SOME_LOOKUP";
			const map = Localization.current();
			map.addTranslation("de", "SOME_LOOKUP", "foo")
			const result = map.translate( key, { warnIfMissing: false } )

			expect( result ).toBeUndefined();
		});
	});
});
