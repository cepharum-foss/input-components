import {isFalsy, isTruthy, sanitizeHtml} from "./utils";

describe("sanitizeHtml()", () => {
	it("returns empty string as provided", () => {
		expect(sanitizeHtml("")).toEqual("");
	});

	it("returns non-HTML string as provided", () => {
		expect(sanitizeHtml("This is regular text.")).toEqual("This is regular text.");
	});

	it("returns basically safe HTML string as provided", () => {
		expect(sanitizeHtml(`This is <a href="file://passwd">a link</a>.`)).toEqual(`This is <a href="file://passwd">a link</a>.`);
	});

	it("keeps special HTML characters by default", () => {
		expect(sanitizeHtml(`If a < b, say "Nö!".`)).toEqual(`If a < b, say "Nö!".`);
	});

	it("prevents injected script from passing", () => {
		expect(sanitizeHtml(`prior <script>reject()</script>`)).toEqual(`prior &lt;script&gt;reject()&lt;/script&gt;`);
		expect(sanitizeHtml(`prior <script src="file.js">reject()</script>`)).toEqual(`prior &lt;script src="file.js"&gt;reject()&lt;/script&gt;`);
		expect(sanitizeHtml(`prior <script src=file.js>reject()</script>`)).toEqual(`prior &lt;script src=file.js&gt;reject()&lt;/script&gt;`);
		expect(sanitizeHtml(`prior <script>reject()`)).toEqual(`prior &lt;script&gt;reject()`);
		expect(sanitizeHtml(`reject()</script>`)).toEqual(`&lt;/script&gt;`);
	});

	it("prevents injected object from passing", () => {
		expect(sanitizeHtml(`prior <object>reject()</object>`)).toEqual(`prior &lt;object&gt;reject()&lt;/object&gt;`);
		expect(sanitizeHtml(`prior <object type="application/pdf">reject()</object>`)).toEqual(`prior &lt;object type="application/pdf"&gt;reject()&lt;/object&gt;`);
		expect(sanitizeHtml(`prior <object type=application/pdf>reject()</object>`)).toEqual(`prior &lt;object type=application/pdf&gt;reject()&lt;/object&gt;`);
		expect(sanitizeHtml(`prior <object>reject()`)).toEqual(`prior &lt;object&gt;reject()`);
		expect(sanitizeHtml(`reject()</object>`)).toEqual(`&lt;/object&gt;`);
	});

	it("prevents injected link from passing", () => {
		expect(sanitizeHtml(`prior <link>reject()</link>`)).toEqual(`prior &lt;link&gt;reject()&lt;/link&gt;`);
		expect(sanitizeHtml(`prior <link rel="script">reject()</link>`)).toEqual(`prior &lt;link rel="script"&gt;reject()&lt;/link&gt;`);
		expect(sanitizeHtml(`prior <link rel=script>reject()</link>`)).toEqual(`prior &lt;link rel=script&gt;reject()&lt;/link&gt;`);
		expect(sanitizeHtml(`prior <link>reject()`)).toEqual(`prior &lt;link&gt;reject()`);
		expect(sanitizeHtml(`reject()</link>`)).toEqual(`&lt;/link&gt;`);
	});

	it("disables any inline event handler", () => {
		expect(sanitizeHtml(`some <span onclick="yourCode()">code</span>!`)).toEqual(`some <span disabled-onclick="yourCode()">code</span>!`);
		expect(sanitizeHtml(`some <span boolean onclick="yourCode()">code</span>!`)).toEqual(`some <span boolean disabled-onclick="yourCode()">code</span>!`);
		expect(sanitizeHtml(`some <span a="b" onclick="yourCode()">code</span>!`)).toEqual(`some <span a="b" disabled-onclick="yourCode()">code</span>!`);
		expect(sanitizeHtml(`some <span onclick="yourCode()" boolean>code</span>!`)).toEqual(`some <span disabled-onclick="yourCode()" boolean>code</span>!`);
		expect(sanitizeHtml(`some <span onclick="yourCode()" b="">code</span>!`)).toEqual(`some <span disabled-onclick="yourCode()" b="">code</span>!`);
		expect(sanitizeHtml(`some <span boolean onclick="yourCode()" test="value">code</span>!`)).toEqual(`some <span boolean disabled-onclick="yourCode()" test="value">code</span>!`);
		expect(sanitizeHtml(`some <span a="b" onclick="yourCode()" test>code</span>!`)).toEqual(`some <span a="b" disabled-onclick="yourCode()" test>code</span>!`);
		expect(sanitizeHtml(`some <span boolean onclick="yourCode()" test="value" oncustom="c(1)">code</span>!`)).toEqual(`some <span boolean disabled-onclick="yourCode()" test="value" disabled-oncustom="c(1)">code</span>!`);
		expect(sanitizeHtml(`some <span a="b" onclick="yourCode()" test oncustom="c(1)">code</span>!`)).toEqual(`some <span a="b" disabled-onclick="yourCode()" test disabled-oncustom="c(1)">code</span>!`);
	});
});


describe("isTruthy()", function () {
		["on", "1", "set", "true", "t", "high", "h", "hi", 200, 1, true]
			.forEach(value => {
				it(`returns boolean true on providing truthy ${typeof value} ${value}`, () => {
					expect(isTruthy(value)).toEqual(true);
					if (typeof value === "string") {
						expect(isTruthy(value.toUpperCase())).toEqual(true);
					}
				});
			});

	["", "off", "0", "clear", "false", "f", "low", "l", "lo", 0, false]
		.forEach(value => {
			it(`returns boolean false on providing falsy ${typeof value} ${value}`, () => {
				expect(isTruthy(value)).toEqual(false);
				if (typeof value === "string") {
					expect(isTruthy(value.toUpperCase())).toEqual(false);
				}
			});
		});
});

describe("isFalsy()", function () {
	["on", "1", "set", "true", "t", "high", "h", "hi", 200, 1, true]
		.forEach(value => {
			it(`returns boolean false on providing truthy ${typeof value} ${value}`, () => {
				expect(isFalsy(value)).toEqual(false);
				if (typeof value === "string") {
					expect(isFalsy(value.toUpperCase())).toEqual(false);
				}
			});
		});

	["", "off", "0", "clear", "false", "f", "low", "l", "lo", 0, false]
		.forEach(value => {
			it(`returns boolean true on providing falsy ${typeof value} ${value}`, () => {
				expect(isFalsy(value)).toEqual(true);
				if (typeof value === "string") {
					expect(isFalsy(value.toUpperCase())).toEqual(true);
				}
			});
		});
});
