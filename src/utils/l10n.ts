/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

const DefaultOptions: TranslationOptions = {
	amount: 1,
	warnIfMissing: true,
};

/**
 * Describes locally managed map of locales into translations per lookup.
 */
export interface TranslationsDatabase {
	[locale: string]: TranslationsPerKey;
}

/**
 * Describes mapping of lookup keys used in internationalized code into related
 * translation(s) of a single locale.
 */
export interface TranslationsPerKey {
	[key: string]: TranslationsPerAmount;
}

/**
 * Describes separate translations per amount (by means of numerus).
 */
export interface TranslationsPerAmount {
	[amount: string]: string;
}

/**
 * Describes single translation for an internationalized key.
 */
export type Translation = string | TranslationsPerAmount;

/**
 * Provides specific lookup providing all supported translations per locale w/o
 * prior registration.
 */
export interface TranslationsPerLocale {
	[locale: string]: Translation;
}

/**
 * Describes internationalized key of translation to look up in local database
 * or some set of translations to use immediately.
 */
export type TranslationSelector = string | TranslationsPerLocale;

/**
 * Tests syntax of locale identifier.
 *
 * FIXME This test is rather strict and might fail to match certain locales.
 */
export const ptnLocale = /^\s*([a-z]{2})(?:[-_]([a-z]{2}))?(?:@[a-z][-a-z0-9]*)?$/i;

/**
 * Describes customizations for looking up particular translation.
 */
export interface TranslationOptions {
	/**
	 * Provides amount to be described in resulting translation.
	 */
	amount?: number;

	/**
	 * Controls whether warning is logged in console when looking up missing
	 * translation.
	 */
	warnIfMissing?: boolean;
}

/**
 * Implements simple i18n/l10n support.
 */
export class Localization {
	/**
	 * Caches local register of translations per supported locale.
	 *
	 * @protected
	 */
	protected translations: TranslationsDatabase = {};

	/**
	 * Selects locale considered current one thus used on looking up
	 * translations.
	 *
	 * @protected
	 */
	protected _locale: string;

	/**
	 * Fetches locale used on l10n currently.
	 */
	get locale(): string {
		return this._locale || ( navigator?.language || "en" ).toLowerCase();
	}

	/**
	 * Explicitly picks a locale to use in l10n further on.
	 *
	 * @param newLocale ID of locale to use in future l10n lookups
	 */
	set locale( newLocale: string ) {
		if ( !ptnLocale.test( newLocale ) ) {
			throw new TypeError( `invalid locale: ${newLocale}` );
		}

		this._locale = newLocale.toLowerCase();
	}

	/**
	 * Detects if provided string obviously contains locale identified.
	 *
	 * @param locale value to check
	 */
	static isLocale( locale ): boolean {
		return ptnLocale.test( locale );
	}

	/**
	 * Adds another translation for single i18n key into selected locale.
	 *
	 * @param locale locale this translation is for
	 * @param key key for addressing this translation in i18n-aware code
	 * @param translation translation for locale/key to register
	 */
	addTranslation(locale: string, key: string, translation: Translation): void {
		if (!ptnLocale.test(locale) ) {
			throw new TypeError("invalid locale");
		}

		if (!key || typeof key !== "string") {
			throw new TypeError("invalid key");
		}

		let map = this.translations[locale];

		if (!map) {
			map = this.translations[locale] = {};
		}

		if (key.startsWith("$")) {
			switch (key) {
				case "$pluralForms" :
					if (typeof translation !== "string") {
						throw new TypeError( "definition of plurals form must be given as string" );
					}

					if ( /[^n0-9=<>?:\s]/.test( translation ) ) {
						throw new TypeError( "definition of plurals form contains invalid character" );
					}

					Object.defineProperty( map, key, { value: new Function( "n", `return ${translation};` ) } );
					break;
			}
		} else {
			if (typeof translation === "string") {
				map[key] = {"*": translation};
			} else if (typeof translation === "object" && translation) {
				map[key] = translation;
			}
		}
	}

	/**
	 * Adds translations for selected locale.
	 *
	 * @param locale locale of provided translations
	 * @param translations translations to add
	 */
	addTranslations( locale: string, translations: TranslationsPerKey ): void {
		for (const key in translations) {
			if (translations.hasOwnProperty(key)) {
				this.addTranslation(locale, key, translations[key]);
			}
		}
	}

	/**
	 * Fetches translation into current locale for provided input.
	 *
	 * @param input selection of previously registered translation or description for immediate translation
	 * @param options lookup customizations
	 * @returns translation of provided input for current locale
	 */
	translate(input: TranslationSelector, options: TranslationOptions = undefined ): string {
		const locale = this._locale || this.locale;
		const context = Object.assign( {}, DefaultOptions, options );
		let translations;

		if ( typeof input === "string" ) {
			translations = ( this.translations[locale] || {} )[input];

			if ( !translations ) {
				translations = ( this.translations[locale.replace( /@.*$/, "" )] || {} )[input]
			}

			if ( !translations ) {
				translations = ( this.translations[locale.replace( /[-_].*$/, "" )] || {} )[input]
			}
		} else if ( typeof input === "object" && input ){
			translations = input;
		}

		if (translations) {
			const mapper = translations.$pluralForms || ( () => 0 );
			if ( typeof mapper === "function" ) {
				return translations[mapper( context.amount || 1 )] || translations["*"];
			}
		}

		if (context.warnIfMissing) {
			console.warn( `missing translation for ${locale}:`, input );
		}

		return undefined;
	}

	/**
	 * Fetches current localization database to use.
	 */
	static current(): Localization {
		const context = ( typeof window !== "undefined" ? window : typeof self !== "undefined" ? self : global ) as any;

		if ( !context.$cepharum$l10n ) {
			context.$cepharum$l10n = new Localization();
		}

		return context.$cepharum$l10n;
	}
}
