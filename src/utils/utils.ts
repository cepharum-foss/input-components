/**
 * Applies most simple HTML sanitization by means of disabling code assumed to
 * cause harm when used in a rendered HTML document without.
 *
 * @param code apparent HTML code to sanitize
 */
export function sanitizeHtml(code: string): string {
	return String( code )
		.replace( /<(\/?)(script|object|link)(\s[^>]*)?>/gi, ( _, closer, name, options ) => {
			return `&lt;${closer}${name}${options}&gt`;
		} )
		.replace( /(<[^>]*?\s)(on\S+=)/g, "$1disabled-$2" );
}

/**
 * Indicates if provided value is resembling boolean true.
 *
 * @param value value to inspect
 */
export function isTruthy(value: any): boolean {
	return /^(?:y(?:es)?|[1-9]\d*|on|h(?:i(?:gh)?)?|set|t(?:rue)?)$/i.test(value);
}

/**
 * Indicates if provided value is resembling boolean false.
 *
 * @param value value to inspect
 */
export function isFalsy(value: any): boolean {
	return /^(?:no?|0|off|l(?:ow?)?|clear|f(?:alse)?|\s*)$/i.test(value);
}

/**
 * Replaces pairs of moustaches (curly braces) with properties of data set
 * selected by name in between the moustaches.
 *
 * @param template text probably containing pairs of moustaches considered placeholders
 * @param data named set of data to inject
 * @returns provided string with occurrences of placeholders replaced by data addressed in provided set
 */
export function moustache(template: string, data = {}): string {
	return template.replace( /{([^}]+)}/g, ( _, name ) => {
		const value = data[name];

		return value == undefined ? "" : value;
	});
}
