/**
 * (c) 2021 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

/**
 * Lists supported normalizations regarding whitespace in a text value.
 */
export enum TextWhitespaceModifications {
	NONE = "",
	TRIM = "trim",
	REDUCE = "reduce",
	ALL = "all",
}

/**
 * Lists supported normalizations regarding case in a text value.
 */
export enum TextCaseModifications {
	NONE = "",
	LOWER = "lower",
	UPPER = "upper,"
}

/**
 * Describes payload of event emitted on change of a control's value due to
 * receiving some input.
 */
export interface InputNotification {
	name?: string;
	value: any;
}

/**
 * Describes common states of a control.
 */
export interface ControlStates {
	touched: boolean;
	changed: boolean;
	valid: boolean;
}

/**
 * Describes payload of event emitted on change of a control's state.
 */
export interface StateNotification {
	name?: string;
	states: ControlStates;
}

/**
 * Combines single error message with indicator on its severity.
 */
export interface TypedError {
	message: string;
	severe: boolean;
}
