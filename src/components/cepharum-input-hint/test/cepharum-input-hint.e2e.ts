import { newE2EPage } from '@stencil/core/testing';

describe('cepharum-input-hint', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<cepharum-input-hint></cepharum-input-hint>');

    const element = await page.find('cepharum-input-hint');
    expect(element).toHaveClass('hydrated');
  });
});
