import { newSpecPage } from '@stencil/core/testing';
import { CepharumInputHint } from '../cepharum-input-hint';

describe('cepharum-input-hint', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [CepharumInputHint],
      html: `<cepharum-input-hint></cepharum-input-hint>`,
    });
    expect(page.root).toEqualHtml(`
      <cepharum-input-hint>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </cepharum-input-hint>
    `);
  });
});
