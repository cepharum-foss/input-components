import {Component, Host, h, Prop} from "@stencil/core";
import {sanitizeHtml} from "../../utils/utils";

/**
 * Renders hint e.g. of an input control.
 */
@Component({
	tag: "cepharum-input-hint",
	styleUrl: "cepharum-input-hint.scss",
	shadow: true,
})
export class CepharumInputHint {

	@Prop() message: string;

	render() {
		return <Host innerHTML={sanitizeHtml(this.message)} />;
	}

}
