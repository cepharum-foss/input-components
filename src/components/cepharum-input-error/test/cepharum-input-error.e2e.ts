import { newE2EPage } from '@stencil/core/testing';

describe('cepharum-input-error', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<cepharum-input-error></cepharum-input-error>');

    const element = await page.find('cepharum-input-error');
    expect(element).toHaveClass('hydrated');
  });
});
