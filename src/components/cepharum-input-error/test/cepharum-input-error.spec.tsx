import { newSpecPage } from '@stencil/core/testing';
import { CepharumInputError } from '../cepharum-input-error';

describe('cepharum-input-error', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [CepharumInputError],
      html: `<cepharum-input-error></cepharum-input-error>`,
    });
    expect(page.root).toEqualHtml(`
      <cepharum-input-error>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </cepharum-input-error>
    `);
  });
});
