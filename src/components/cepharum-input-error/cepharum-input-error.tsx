import {Component, Host, h, Prop} from "@stencil/core";

@Component({
	tag: "cepharum-input-error",
	styleUrl: "cepharum-input-error.scss",
	shadow: true,
})
export class CepharumInputError {

	@Prop() message: string | string[];

	render() {
		const errors = Array.isArray( this.message ) ? this.message : this.message ? [this.message] : [];

		return (
			<Host>
				<div class="errors">
					{errors.map( error => <span class="error">{error}</span> )}
				</div>
			</Host>
		);
	}

}
