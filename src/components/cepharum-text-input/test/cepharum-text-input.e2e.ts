import { newE2EPage } from '@stencil/core/testing';

describe('cepharum-text-input', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<cepharum-text-input></cepharum-text-input>');

    const element = await page.find('cepharum-text-input');
    expect(element).toHaveClass('hydrated');
  });
});
