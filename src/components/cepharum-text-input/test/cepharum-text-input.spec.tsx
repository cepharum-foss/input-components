import {newSpecPage} from "@stencil/core/testing";
import {CepharumTextInput} from "../cepharum-text-input";

describe("cepharum-text-input", () => {
	it("renders", async () => {
		const page = await newSpecPage({
			components: [CepharumTextInput],
			html: `<cepharum-text-input></cepharum-text-input>`,
		});
		expect(page.root).toEqualHtml(`
      <cepharum-text-input>
        <cepharum-input type="text">
          <slot name="label"></slot>
          <slot name="error"></slot>
          <slot name="hint"></slot>
          <slot>
            <input type="text">
          </slot>
		</cepharum-input>
      </cepharum-text-input>
    `);
	});
});
