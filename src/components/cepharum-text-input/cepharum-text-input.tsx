import {
	Component,
	Event,
	EventEmitter,
	h,
	Method,
	Prop,
	State,
	Watch
} from "@stencil/core";
import {
	InputNotification,
	StateNotification,
	TextCaseModifications,
	TextWhitespaceModifications, TypedError
} from "../../utils/types";
import {Localization} from "../../utils/l10n";
import {moustache} from "../../utils/utils";

/**
 * Implements regular single-line text input field.
 */
@Component({
	tag: "cepharum-text-input",
	styleUrl: "cepharum-text-input.scss",
	shadow: true,
})
export class CepharumTextInput {

	/**
	 * Exposes localization to use.
	 */
	@Prop() l10n: Localization = Localization.current();

	/**
	 * Names this control in context of a larger context including it.
	 */
	@Prop() name: string;

	/**
	 * Provides field's current value.
	 */
	@Prop({mutable: true}) value: any = "";

	/**
	 * Controls whether any input is concealed e.g. for entering token.
	 */
	@Prop({reflect: true}) concealed: boolean = false;

	/**
	 * Provides label rendered next to this control.
	 */
	@Prop() label: string = undefined;

	/**
	 * Provides one or more error messages to show in relation to this control.
	 */
	@Prop() error: string | string[] = undefined;

	/**
	 * Provides a message to show for explaining this control's intention.
	 */
	@Prop() hint: string = undefined;

	/**
	 * Indicates if some non-empty value is required or not.
	 */
	@Prop({reflect: true}) required: boolean = false;

	/**
	 * Selects required minimum number of characters accepted.
	 */
	@Prop() minLength: number = 0;

	/**
	 * Selects required maximum number of characters accepted.
	 */
	@Prop() maxLength: number = Infinity;

	/**
	 * Controls implicit processing of whitespace included with control's value.
	 */
	@Prop() whitespace: TextWhitespaceModifications = TextWhitespaceModifications.NONE;

	/**
	 * Controls implicit modification of text case of control's value.
	 */
	@Prop() case: TextCaseModifications = TextCaseModifications.NONE;

	/**
	 * Controls whether some causes for invalid state of control should not
	 * result in display of error.
	 */
	@Prop() relax: boolean = false;


	/**
	 * Tracks if value of control is complying with its defined conditions.
	 */
	@State() valid: boolean;

	/**
	 * Tracks if control has received any input since it has been mounted.
	 */
	@State() changed: boolean = false;

	/**
	 * Tracks if control has been focused recently
	 */
	@State() touched: boolean = false;

	/**
	 * Lists errors to display.
	 */
	@State() errors: TypedError[] = [];


	@Event({
		eventName: "value-changed",
		bubbles: true,
		cancelable: true,
		composed: true,
	}) valueChangedNotification: EventEmitter<InputNotification>;

	@Event({
		eventName: "state-changed",
		bubbles: true,
		cancelable: true,
		composed: true,
	}) stateChangedNotification: EventEmitter<StateNotification>;


	protected textField: HTMLInputElement;


	/**
	 * Manages applicable conditions parsed from properties.
	 *
	 * @protected
	 */
	protected conditions = {
		required: false,
		minLength: 0,
		maxLength: Infinity,
	};

	/**
	 * Manages applicable conditions parsed from properties.
	 *
	 * @protected
	 */
	protected normalizations = {
		trim: TextWhitespaceModifications.NONE,
		case: TextCaseModifications.NONE,
	};


	render() {
		const {concealed, name, label, hint, value} = this;
		const type = concealed ? "password" : "text";
		const formatted = this.format(value);
		const errors = this.errors
			.filter( error => !this.relax || error.severe)
			.map( error => error.message );

		return (
			<cepharum-input name={name || undefined} type={type} class={{
				touched: this.touched,
				untouched: !this.touched,
				changed: this.changed,
				pristine: !this.changed,
				valid: this.valid,
				invalid: !this.valid,
				"has-errors": errors.length > 0,
			}}>
				<slot name="label">{
					label ? <cepharum-input-label name={name}
												  label={label}/> : undefined
				}</slot>
				<slot name="error">{
					errors.length > 0 ? <cepharum-input-error message={errors}/> : undefined
				}</slot>
				<slot name="hint">{
					hint ? <cepharum-input-hint message={hint}/> : undefined
				}</slot>
				<slot>
					<input ref={el => {this.textField = el;}}
						   type={type} value={formatted}
						   onFocus={() => this.touch()}
						   onInput={event => this.onInput(event)}
						   onBlur={() => this.onBlur()}
						   maxlength={this.maxLength > 0 ? ~this.maxLength : undefined}/>
				</slot>
			</cepharum-input>
		);
	}

	componentWillLoad(): void {
		this.onMinLengthChanged(this.minLength, undefined, true);
		this.onMaxLengthChanged(this.maxLength, undefined, true);
		this.onWhitespaceChanged(this.whitespace, undefined, true);
		this.onCaseChanged(this.case, undefined, true);

		this.revalidate(this.value, true);
	}

	/**
	 * Normalizes provided value according to this control's configuration.
	 *
	 * @param value value to be normalized
	 * @returns normalized value
	 * @protected
	 */
	protected normalize(value: any): string {
		let result = String( value );

		switch ( this.normalizations.trim ) {
			case TextWhitespaceModifications.TRIM :
				result = result.trim();
				break;

			case TextWhitespaceModifications.REDUCE :
				result = result.replace( /\s+/g, " " );
				break;

			case TextWhitespaceModifications.ALL :
				result = result.trim().replace( /\s+/g, " " );
				break;

			default :
		}

		return result;
	}

	/**
	 * Validates provided value listing all encountered issues on return.
	 *
	 * @param value value to inspect
	 * @returns list of encountered issues
	 * @protected
	 */
	protected validate(value: string): TypedError[] {
		const errors: TypedError[] = [];

		if (String(value).length < 1) {
			if (this.required) {
				errors.push({
					message: this.l10n.translate("TEXT.ERROR.REQUIRED") || "This information is required.",
					severe: false,
				});
			}
		} else {
			if (this.value.length < this.conditions.minLength) {
				const error = this.l10n.translate("TEXT.ERROR.MIN_LENGTH") || "This information must consist of {required} character(s) at least.";
				const data = {
					actual: this.value.length,
					required: this.conditions.minLength,
					missing: this.conditions.minLength - this.value.length,
				};

				errors.push({
					message: moustache(error, data),
					severe: false,
				});
			}

			if (this.value.length > this.conditions.maxLength) {
				const error = this.l10n.translate("TEXT.ERROR.MAX_LENGTH") || "This information may consist of {required} character(s) at most.";
				const data = {
					actual: this.value.length,
					required: this.conditions.minLength,
					missing: this.conditions.minLength - this.value.length,
				};

				errors.push({
					message: moustache(error, data),
					severe: false,
				});
			}
		}

		return errors;
	}

	protected format(value: string): string {
		if (value == undefined) {
			return "";
		}

		return value;
	}

	/**
	 * Normalizes provided value and adopts it as control's current value prior
	 * to validating it and optionally emitting notification on actual change of
	 * state.
	 *
	 * @param value new value of control to be normalized and validated
	 * @param silent set true to prevent notification on change of state
	 * @returns true if validity of control has changed
	 * @protected
	 */
	protected revalidate(value: any, silent: boolean = false): boolean {
		const wasValid = this.valid;

		this.value = this.normalize(value);
		this.errors = this.validate(this.value);
		this.valid = this.errors.length < 1;

		const changed = this.valid !== wasValid;

		if ( !silent && changed ) {
			this.stateChangedNotification.emit({
				name: this.name,
				states: {
					touched: this.touched,
					changed: this.changed,
					valid: this.valid,
				},
			});
		}

		return changed;
	}

	/**
	 * Handles control's mark for being touched at least once by means of
	 * gaining focus.
	 */
	touch(): void {
		if (!this.touched) {
			this.touched = true;

			this.stateChangedNotification.emit({
				name: this.name,
				states: {
					touched: this.touched,
					changed: this.changed,
					valid: this.valid,
				},
			});
		}
	}

	/**
	 * Handles change of embedded field's value by adopting it as new value of
	 * control to be normalized and validated.
	 *
	 * @param event event emitted by embedded input field on change of its value
	 */
	onInput(event): void {
		const oldValue = this.value;
		const newValue = event.target.value;

		if ( newValue !== oldValue ) {
			let stateChanged = this.revalidate(newValue, !this.changed);

			const valueChanged = this.value !== oldValue;

			if (!this.changed && valueChanged) {
				stateChanged = true;
				this.changed = true;
			}

			if (stateChanged) {
				this.stateChangedNotification.emit({
					name: this.name,
					states: {
						touched: this.touched,
						changed: this.changed,
						valid: this.valid,
					},
				});
			}

			if (valueChanged) {
				this.valueChangedNotification.emit({
					name: this.name,
					value: this.value,
				});
			}
		}
	}

	/**
	 * Updates field on losing focus.
	 */
	onBlur(): void {
		this.revalidate(this.value);

		// e.g. on trimming enabled, value isn't reassigned by render() which is
		// invoked after this -> update value explicitly here
		this.textField.value = this.format(this.value);
	}

	@Watch("required")
	onRequiredChanged(): void {
		this.revalidate(this.value);
	}

	@Watch("minLength")
	onMinLengthChanged(length: any, _old: any, skipValidation?: boolean): void {
		if (length == undefined) {
			this.conditions.minLength = 0;
		} else {
			const parsed = typeof length === "number" ? length : parseInt(String(length));
			if (parsed > -1) {
				if (parsed > this.conditions.maxLength) {
					throw new TypeError(`${this.name || "unnamed control"}: minimum length is too long for maximum length`);
				} else {
					this.conditions.minLength = parsed;
				}
			} else {
				throw new TypeError(`${this.name || "unnamed control"}: minimum length must be 0 at least`);
			}
		}

		if ( !skipValidation ) {
			this.revalidate(this.value);
		}
	}

	@Watch("maxLength")
	onMaxLengthChanged(length: any, _old: any, skipValidation?: boolean): void {
		if (length == undefined) {
			this.conditions.maxLength = Infinity;
		} else {
			const parsed = typeof length === "number" ? length : parseInt(String(length));
			if (parsed > 0) {
				if (parsed < this.conditions.minLength) {
					throw new TypeError(`${this.name || "unnamed control"}: maximum length is too short for minimum length`);
				} else {
					this.conditions.maxLength = parsed;
				}
			} else {
				throw new TypeError(`${this.name || "unnamed control"}: maximum length must be 1 at least`);
			}
		}

		if ( !skipValidation ) {
			this.revalidate(this.value);
		}
	}

	@Watch("whitespace")
	onWhitespaceChanged(mode: any, _old: any, skipValidation?: boolean): void {
		if (mode == undefined) {
			this.normalizations.trim = TextWhitespaceModifications.NONE;
		} else {
			const value = String(mode).trim().toLowerCase()
			switch (value) {
				case TextWhitespaceModifications.NONE :
				case TextWhitespaceModifications.TRIM :
				case TextWhitespaceModifications.REDUCE :
				case TextWhitespaceModifications.ALL :
					this.normalizations.trim = value;
					break;

				default :
					throw new TypeError(`${this.name || "unnamed control"}: invalid mode for handling whitespace`);
			}
		}

		if ( !skipValidation ) {
			this.revalidate(this.value);
		}
	}

	@Watch("case")
	onCaseChanged(mode: any, _old: any, skipValidation?: boolean): void {
		if (mode == undefined) {
			this.normalizations.case = TextCaseModifications.NONE;
		} else {
			const value = String(mode).trim().toLowerCase()
			switch (value) {
				case TextCaseModifications.NONE :
				case TextCaseModifications.LOWER :
				case TextCaseModifications.UPPER :
					this.normalizations.case = value;
					break;

				default :
					throw new TypeError(`${this.name || "unnamed control"}: invalid mode for handling text case`);
			}
		}

		if ( !skipValidation ) {
			this.revalidate(this.value);
		}
	}

	@Watch("relax")
	onRelaxChanged(): void {
		this.revalidate(this.value);
	}

	@Watch("value")
	onValueChanged(value: any, _old: any, skipValidation?: boolean): void {
		if ( !skipValidation ) {
			this.revalidate(value);
		}
	}

	/**
	 * Fetches control's current state of having accepted value.
	 */
	@Method()
	async isValid(): Promise<boolean> {
		return this.valid;
	}

	/**
	 * Fetches control's current state of having received any input since
	 * mounting.
	 */
	@Method()
	async hasChanged(): Promise<boolean> {
		return this.changed;
	}
}
