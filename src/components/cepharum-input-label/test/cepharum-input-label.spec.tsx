import { newSpecPage } from '@stencil/core/testing';
import { CepharumInputLabel } from '../cepharum-input-label';

describe('cepharum-input-label', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [CepharumInputLabel],
      html: `<cepharum-input-label></cepharum-input-label>`,
    });
    expect(page.root).toEqualHtml(`
      <cepharum-input-label>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </cepharum-input-label>
    `);
  });
});
