import { newE2EPage } from '@stencil/core/testing';

describe('cepharum-input-label', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<cepharum-input-label></cepharum-input-label>');

    const element = await page.find('cepharum-input-label');
    expect(element).toHaveClass('hydrated');
  });
});
