import {Component, h, Prop} from "@stencil/core";

@Component({
	tag: "cepharum-input-label",
	styleUrl: "cepharum-input-label.scss",
	shadow: true,
})
export class CepharumInputLabel {
	@Prop() name: string;
	@Prop() label: string;

	render() {
		return <label htmlFor={this.name}>{this.label}</label>;
	}
}
