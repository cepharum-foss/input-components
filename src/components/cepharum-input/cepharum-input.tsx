import {Component, h, Prop, Watch} from "@stencil/core";

/**
 * Implements common functionality of input components.
 */
@Component({
	tag: "cepharum-input",
	styleUrl: "cepharum-input.scss",
	shadow: true,
})
export class CepharumInput {

	/**
	 * Selects type of input control to indicate in resulting HTML.
	 */
	@Prop() type!: string;

	/**
	 * Provides name of input control.
	 */
	@Prop() name: string;

	/**
	 * Provides visible label of input control.
	 */
	@Prop() label: string;

	/**
	 * Provides list of error messages to appear in relation to input control.
	 */
	@Prop() error: string[];

	/**
	 * Provides additional hints on how to use this input control.
	 */
	@Prop() hint: string;

	render() {
		const {type, name, label, error, hint} = this;
		const errors = Array.isArray(error) ? error : error ? [error] : [];

		return (
			<div class={`${type}-field field`}>
				<div class="label">
					<slot name="label">
						{label && name ?
							<label htmlFor={name}>{label}</label> : label ?
								<label
									htmlFor={name}>{label}</label> : undefined}
					</slot>
				</div>
				<div class="content">
					<slot/>
					<slot name="error">
						{errors.length ? <div class="errors">
							{errors.map(error =>
								<p class="error">{error}</p>
							)}
						</div> : undefined}
					</slot>
					<slot name="hint">
						{hint ? <div class="hint">{hint}</div> : undefined}
					</slot>
				</div>
			</div>
		);
	}

	componentWillLoad(): void {
		this.onTypeChanged(this.type);
		this.onNameChanged(this.name);
	}

	@Watch("type")
	onTypeChanged( type: string ): void {
		if ( type && /^[a-z_$][a-z0-9_$]*(?:\[])$/i.test( type ) ) {
			throw new TypeError( "invalid input control type" );
		}
	}

	@Watch("name")
	onNameChanged( name: string ): void {
		if ( name && /^[a-z_$][a-z0-9_$]*(?:\[])$/i.test( name ) ) {
			throw new TypeError( "invalid input control name" );
		}
	}
}
