import {newSpecPage} from '@stencil/core/testing';
import {CepharumInput} from '../cepharum-input';

describe('cepharum-input', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [CepharumInput],
      html: `<cepharum-input></cepharum-input>`,
    });
    expect(page.root).toEqualHtml(`
      <cepharum-input>
        <mock:shadow-root>
          <div class="field undefined-field">
            <div class="label">
              <slot name="label"></slot>
            </div>
            <div class="content">
              <slot></slot>
              <slot name="error"></slot>
              <slot name="hint"></slot>
            </div>
          </div>
        </mock:shadow-root>
      </cepharum-input>
    `);
  });
});
