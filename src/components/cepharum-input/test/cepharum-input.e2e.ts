import { newE2EPage } from '@stencil/core/testing';

describe('cepharum-input', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<cepharum-input></cepharum-input>');

    const element = await page.find('cepharum-input');
    expect(element).toHaveClass('hydrated');
  });
});
